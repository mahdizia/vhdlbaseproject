----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:02:35 08/26/2020 
-- Design Name: 
-- Module Name:    sevensegmentdecoder - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sevensegmentdecoder is
    Port ( Digit : in  STD_LOGIC_VECTOR (3 downto 0);
           segment_A : out  STD_LOGIC;
           segment_B : out  STD_LOGIC;
           segment_C : out  STD_LOGIC;
           segment_D : out  STD_LOGIC;
           segment_E : out  STD_LOGIC;
           segment_F : out  STD_LOGIC;
           segment_G : out  STD_LOGIC);
end sevensegmentdecoder;

architecture Behavioral of sevensegmentdecoder is

begin
process(Digit)
			variable decoder_data : STD_LOGIC_VECTOR (6 downto 0);
			begin
			case Digit is
				when "0000" => decoder_data := "1111110";
				when "0001" => decoder_data := "0110000";
				when "0010" => decoder_data := "1101101";
				when "0011" => decoder_data := "1111001";
				when "0100" => decoder_data := "0110011";
				when "0101" => decoder_data := "1011011";
				when "0110" => decoder_data := "1011111";
				when "0111" => decoder_data := "1110000";
				when "1000" => decoder_data := "1111111";
				when "1001" => decoder_data := "1111011";
				when "1010" => decoder_data := "1111011";
				when "1011" => decoder_data := "0011111";
				when "1100" => decoder_data := "1001110";
				when "1101" => decoder_data := "0111101";
				when "1110" => decoder_data := "1001111";
				when "1111" => decoder_data := "1000111";
				when others => decoder_data := "0111110";
			end case;
			segment_A <= not decoder_data(6);
			segment_B <= not decoder_data(5);
			segment_C <= not decoder_data(4);
			segment_D <= not decoder_data(3);
			segment_E <= not decoder_data(2);
			segment_F <= not decoder_data(1);
			segment_G <= not decoder_data(0);
end process;







end Behavioral;

