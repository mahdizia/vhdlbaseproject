library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
USE ieee.std_logic_arith.ALL;

entity TopDesign is
    Port ( 
			Trigger	: in  STD_LOGIC;
			Ultrasonic_Data_for_send : inout  STD_LOGIC_VECTOR (7 downto 0);
			Echo	: out  STD_LOGIC;

			TX2Ar_AR_Uart_tx: out  STD_LOGIC;
		   AR_Uart_BlinkLed : out STD_LOGIC;  
			RX2AR_AR_Uart_rx: in  STD_LOGIC;

			FPGA_PFGA_CLK : in std_logic;

			Alarm_LED : out STD_LOGIC;

			PC_Uart_rx: in  STD_LOGIC;
	      PC_Uart_tx: out  STD_LOGIC;
		   PC_Uart_BlinkLed : out STD_LOGIC 
			  );
end TopDesign;


architecture Behavioral of TopDesign is

component Range_Sensor is
    Port ( 
		  fpgaclk 		: in  STD_LOGIC;
          pulse 		: in  STD_LOGIC;
          triggerOut 	: out  STD_LOGIC;
          meters 		: out  STD_LOGIC_VECTOR (3 downto 0);
          decimeters 	: out  STD_LOGIC_VECTOR (3 downto 0);
          centimeters 	: out  STD_LOGIC_VECTOR (3 downto 0);
		  led     		: out  STD_LOGIC;
		  Data_for_send : out  STD_LOGIC_VECTOR (7 downto 0)
			  );
end component;


component uart_transceiver is
	 port (
          sys_rst	: in  STD_LOGIC;
	      sys_clk	: in  STD_LOGIC;

	      uart_rx	: in  STD_LOGIC;
	      uart_tx	: out STD_LOGIC;
	      divisor 	: in  STD_LOGIC_VECTOR(15 downto 0);--Clk_Freq/(Baud*16)

	      rx_data	: out STD_LOGIC_VECTOR(7 downto 0);
          rx_done	: out STD_LOGIC;

	      tx_data	: in  STD_LOGIC_VECTOR(7 downto 0);
	      tx_wr		: in  STD_LOGIC;
	      tx_done	: out STD_LOGIC
		  );
end component;


signal Data_Ultrasonic_Meters: STD_LOGIC_VECTOR (3 downto 0);
signal Data_Ultrasonic_Decimeters: STD_LOGIC_VECTOR (3 downto 0);
signal Data_Ultrasonic_Centimeters: STD_LOGIC_VECTOR (3 downto 0);

signal PC_Uart_Counter  : std_logic_vector(24 downto 0):=(others=>'0');
signal PC_Uart_rx_data:STD_LOGIC_VECTOR(7 downto 0);
signal PC_Uart_rx_done:STD_LOGIC;
signal PC_Uart_tx_wr:STD_LOGIC:='0';
signal PC_Uart_sys_rst:std_logic;

signal Data_for_send:STD_LOGIC_VECTOR(7 downto 0);

signal Si_AR_Uart_Counter   :STD_LOGIC_VECTOR(24 downto 0):=(others=>'0');
signal Si_AR_Uart_rx_data   :STD_LOGIC_VECTOR(7 downto 0);
signal Si_AR_Uart_rx_done   :STD_LOGIC;
signal Si_AR_Uart_tx_wr     :STD_LOGIC:='0';
signal Si_AR_Uart_sys_rst   :STD_LOGIC;
signal Si_AR_Uart_rx_data_AR_temp   :STD_LOGIC_VECTOR(7 downto 0);

CONSTANT Ultrasonic_Int_UpperBound : integer := 0;
CONSTANT Ultrasonic_Int_LowerBound : integer := 0;

type state is (Idel,S0,S1);
signal Sstate : state;

begin

Ultrasonic_uut4: Range_Sensor port map(
			fpgaclk => FPGA_PFGA_CLK,
			pulse => Trigger,
			triggerOut => Echo,
            meters => Data_Ultrasonic_Meters,
            decimeters => Data_Ultrasonic_Decimeters,
            centimeters => Data_Ultrasonic_Centimeters,
		    led => Alarm_LED,
		    Data_for_send => Ultrasonic_Data_for_send
);


Si_AR_Uart_sys_rst <= '0';
uart_transceiver_U0_AR:uart_transceiver port map
    (
     sys_rst  => Si_AR_Uart_sys_rst,
	 sys_clk => FPGA_PFGA_CLK,

	 uart_tx  => TX2Ar_AR_Uart_tx,
	 uart_rx  => RX2AR_AR_Uart_rx,

	divisor  => x"009C",--Clk_Freq/(Baud*16)

	rx_data  => Si_AR_Uart_rx_data_AR_temp , 
    rx_done  => Si_AR_Uart_rx_done,

	 tx_data  => Si_AR_Uart_rx_data_AR_temp,
--	 tx_data  => "00110101",
	 tx_wr    => Si_AR_Uart_tx_wr ,
	 tx_done  => open
);
process(FPGA_PFGA_CLK)
begin
    if(FPGA_PFGA_CLK='1' and FPGA_PFGA_CLK'event)then
		if(si_AR_Uart_rx_done ='1' ) then
			si_AR_Uart_rx_data <= Si_AR_Uart_rx_data_AR_temp;
--			si_AR_Uart_rx_data <= "00000011";
		end if;
	 end if;
end process;

Si_AR_Uart_tx_wr <= Si_AR_Uart_rx_done;
Si_AR_Uart_rx_data <= Si_AR_Uart_rx_data;
AR_Uart_BlinkLed <= Si_AR_Uart_Counter(24) when Si_AR_Uart_sys_rst='0' else '1';
process(FPGA_PFGA_CLK)
begin
    if(FPGA_PFGA_CLK='1' and FPGA_PFGA_CLK'event)then
	     Si_AR_Uart_Counter <= Si_AR_Uart_Counter + 1;			  
	end if;
end process;

PC_Uart_sys_rst <= '0';
--Devisor=Clk_freq/(16*BaudRate)

--Data_for_send <= Ultrasonic_Data_for_send;


process(FPGA_PFGA_CLK)
begin
    if(FPGA_PFGA_CLK='1' and FPGA_PFGA_CLK'event)then
	     case Sstate is
				when Idel => 
					if(Si_AR_Uart_rx_done = '1') then
						Data_for_send <= si_AR_Uart_rx_data;
						PC_Uart_tx_wr <= '1';
						Sstate <= S0;
					end if;
				
				when S0 => 
					PC_Uart_tx_wr <= '1';
					Data_for_send <= Ultrasonic_Data_for_send;
					Sstate <= S1;
				
				when S1 => 
					PC_Uart_tx_wr <= '0';
					Sstate <= Idel;
			end case;
	end if;
end process;


PC_uart_transceiver_U0:uart_transceiver port map
    (
     sys_rst  => PC_Uart_sys_rst,
	 sys_clk => FPGA_PFGA_CLK,

	 uart_rx  => PC_Uart_rx,
	 uart_tx  => PC_Uart_tx,
	 divisor  => x"009C",--Clk_Freq/(Baud*16)

	 rx_data  => PC_Uart_rx_data, 
     rx_done  => PC_Uart_rx_done,

	 tx_data  => Data_for_send,
	 tx_wr    => PC_Uart_tx_wr,
	 tx_done  => open
);

PC_Uart_rx_data <= PC_Uart_rx_data;
PC_Uart_BlinkLed <= PC_Uart_Counter(24) when PC_Uart_sys_rst='0' else '1';
process(FPGA_PFGA_CLK)
begin
    if(FPGA_PFGA_CLK='1' and FPGA_PFGA_CLK'event)then
	     PC_Uart_Counter <= PC_Uart_Counter + 1;			  
	end if;
end process;

end Behavioral;

